import codecs
import re
import os

from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))

package = "pywordle"
requirements = []
test_requirements = ['tox']


def read(*parts):
    return codecs.open(os.path.join(here, *parts), 'r').read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name="pywordle",
    author="James O'Hara",
    packages=['pywordle'],
    setup_requires=['wheel'],
    install_requires=requirements,
    scripts=['bin/wordle'],
    test_suite='tests',
    version=find_version("pywordle", "__init__.py"),
    classifiers=[
        'Development Status :: 5 - PreProduction/Beta',
        'Intended Audience :: Anyone',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8'
    ],
    python_requires='>=3.6',
)
