# Py-Wordle
by James O'Hara

## To install:

1. Clone this repo
2. Switch to a python 3.8 venv
3. `pip install .`

## To Run:
Type `wordle` then enter

## To Unit Test:
1. `pip install tox`
2. `tox`