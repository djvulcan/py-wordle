#!/usr/env/python

from pywordle.wordbank import Wordbank


class WordMap():
    def __init__(self, word):
        self.word = word.upper()
        self.map = []
        self.is_valid = False
        self.__Validate__()
        for i in range(len(self.word)):
            self.map.append(
                {
                    'letter': self.word[i],
                    'processed': False,
                    'position': i
                }
            )

    def Reset(self):
        for letter in self.map:
            letter['processed'] = False

    def __str__(self):
        return str(self.map)

    def __Validate__(self):
        if len(self.word) == 5:
            self.is_valid = True


class Word():
    def __init__(self, word):
        self.word = word
        self.word_map = WordMap(self.word)

    def __str__(self):
        return self.word

    def Guess(self, guess_word):
        self.word_map.Reset()
        guess_map = WordMap(guess_word)
        greenresult = ''
        yellowresult = ''
        result = ''
        for guess_letter in guess_map.map:
            greenresult += self.__GuessLetterGreen__(guess_letter)
        for guess_letter in guess_map.map:
            yellowresult += self.__GuessLetterYellow__(guess_letter)
        for i in range(len(greenresult)):
            if greenresult[i] == '⬛' and yellowresult[i] == '🟨':
                result += yellowresult[i]
            else:
                result += greenresult[i]
        return result

    def __GuessLetterGreen__(self, guess_letter):
        for word_letter in self.word_map.map:
            result = '⬛'
            if not word_letter['processed']:
                if word_letter['letter'] == guess_letter['letter']:
                    if word_letter['position'] == guess_letter['position']:
                        result = '🟩'
                        word_letter['processed'] = True
                    break
        return result

    def __GuessLetterYellow__(self, guess_letter):
        for word_letter in self.word_map.map:
            result = '⬛'
            if not word_letter['processed']:
                if word_letter['letter'] == guess_letter['letter']:
                    if word_letter['position'] != guess_letter['position']:
                        result = '🟨'
                        word_letter['processed'] = True
                    break
        return result


class Game():
    def __init__(self):
        self.turns = 6
        self.result = 'You ran out of turns.  Better luck next time'
        word_bank = Wordbank()
        the_word = Word(word_bank.random())
        self.transcript = ''
        for i in range(self.turns):
            valid = False
            while not valid:
                guess = input(f'Guess a Word  ({i+1}/6):\n\n').upper()
                valid = word_bank.validate(guess)
                if not valid:
                    print('Invalid Word! Please try again')
            valid = False
            guess_result = the_word.Guess(guess)
            self.transcript += f'{guess_result}\n'
            print(self.__GuessOutput__(guess, guess_result))
            if guess_result == '🟩🟩🟩🟩🟩':
                self.result = f'Congratulations - you got it in {i+1}/6'
                break
        print(self.result)
        print(f'\n{self.transcript}')

    def __GuessOutput__(self, word, result):
        output = ''
        for i in range(len(word)):
            char = f'\033[1;37;100m{word[i]}'
            if result[i] == '🟩':
                char = f'\033[1;37;102m{word[i]}'
            elif result[i] == '🟨':
                char = f'\033[1;37;103m{word[i]}'
            output += char
        output += '\033[0m'
        return output


def main():
    print("PY-WORDLE")
    print("=========\n")
    print("By James O'Hara, 2022\n")
    Game()


if __name__ == "__main__":
    main()
