from pywordle.wordle import Word

from unittest import TestCase


class TestWordle(TestCase):
    def test_correct_guess_works(self):
        """
        Test that a correct guess works
        """
        test_word = Word('ALLOW')
        test_guess = test_word.Guess('ALLOW')
        assert test_guess == '🟩🟩🟩🟩🟩'

    def test_incorrect_guess_works(self):
        """
        Test that an incorrect guess works
        """
        test_word = Word('ALLOW')
        test_guess = test_word.Guess('QTERT')
        assert test_guess == '⬛⬛⬛⬛⬛'

    def test_some_correctly_placed_letters(self):
        """
        Test some correctly placed letters
        """
        test_word = Word('AMLOW')
        test_guess = test_word.Guess('EFLOT')
        assert test_guess == '⬛⬛🟩🟩⬛'

    def test_some_incorrectly_placed_letters(self):
        """
        Test some incorrectly placed letters
        """
        test_word = Word('ABCDE')
        test_guess = test_word.Guess('BADEC')
        assert test_guess == '🟨🟨🟨🟨🟨'

    def test_some_duplicate_correctly_placed_letters(self):
        """
        Test some duplicate correctly placed letters
        """
        test_word = Word('FERET')
        test_guess = test_word.Guess('FFPOT')
        assert test_guess == '🟩⬛⬛⬛🟩'

    def test_greens_before_yellows(self):
        """
        Test green letters prioritised over yellow
        """
        test_word = Word('ALLOW')
        test_guess = test_word.Guess('ALOOF')
        assert test_guess == '🟩🟩⬛🟩⬛'

    def test_greens_before_yellows_with_yellows(self):
        """
        Test green letters prioritised over yellow with valid yellows
        """
        test_word = Word('ALLOW')
        test_guess = test_word.Guess('AWOOF')
        assert test_guess == '🟩🟨⬛🟩⬛'
